#!/usr/bin/env bash

git restore .
git clean -xfd 

pip3 install reuse
git switch -c license

rm -rf LICENSE*
rm -rf documentation/licenses
find . -iname "dco.txt" -delete

git stage .
git commit -m "docs: delete LICENSE*"

reuse download GPL-3.0-only
cp LICENSES/GPL-3.0-only.txt LICENSE

lynx --dump https://developercertificate.org/ > DCO-1.1.txt

git stage .
git commit -m "docs: add LICENSE and DCO"
git push -u origin license
