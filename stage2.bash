#!/usr/bin/env bash

git switch main
git pull
git branch -D license
git switch -c license
reuse download CC-BY-SA-4.0
git stage .
git commit -m "docs: add CC-BY-SA-4.0 for content"
git push -u origin license
