# Fix Licenses

GitLab is not correctly detecting our license (GPL-3.0) in many of our
projects. This project provides a procedure and a couple scripts to help
correct this.

## Procedure

The following 2-stage procedure will likely work for many repositories:

1. Open repository in a GitPod.io workspace
2. Run the following in terminal in workspace
    ```bash
    git clone https://gitlab.com/LibreFoodPantry/common-services/fixlicenses.git /home/gitpod/fixlicenses
    /home/gitpod/fixlicenses/stage1.bash
    ```
3. Create and merge merge request deleting `license` branch after merge.
4. Return to project on GitLab and confirm license pill shows "GNU GPLv3".
5. Run the following in the same GitPod.io session
    ```bash
    /home/gitpod/fixlicenses/stage2.bash
    ```
6. Create and merge merge request deleting `license` branch after merge.
7. Return to project on GitLab and confirm license pill shows "GNU GPLv3".
8. Stop and delete GitPod.io workspace
